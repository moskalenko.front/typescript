//Decorator
function logger(constFn: Function) {
    constFn.prototype.showObj = function () {
        console.log(this);
    }
}

@logger
class Man {
    constructor(
        public name: string,
        public age: number
    ) {}
}

const man = new Man('Slon', 1);
(<any>man).showObj();
//

//Generic
function genericGetter<T>(data: T): T {
    return data;
}
let newGenericFunction: <T>(data: T) => T = genericGetter;
console.log(newGenericFunction('string'));
console.log(newGenericFunction(123));

class Multiply<T extends number | string> {
    constructor(private a: T, private b: T) {}

    public getResult(): number {
        return +this.a * +this.b;
    }
}

let multiplyNum = new Multiply(10, 20);
console.log(multiplyNum.getResult());
let multiplyStr= new Multiply('210', '20');
console.log(multiplyStr.getResult());
//

//Interface
interface IHuman {
    name: string;
    age: number;
    home: boolean;
    hair?: string;
}

const human: IHuman = {
    name: 'Slon',
    age: 150,
    home: true,
}

class Human implements IHuman {
    constructor(
        public name: string, 
        public age: number, 
        public home: boolean, 
        public hair: string
    ){}
}
//

//Class
abstract class Car {
    constructor(public model: string, public year: number){}
    private body: string = 'sedan';
    abstract logInfo(info: string): void;

    getCarYear(): number {
        return this.year;
    }
}

class Mercedes extends Car {
    methodMercedes() {
        console.log();
    }
    logInfo(info: string): void{
        console.log(info);
    }
}
let car = new Mercedes("Mercedes", 2011);
console.log(car);

class User {
    private job: string = 'default';
    protected flag: boolean = true;
    constructor(public name: string, public age: number){}
    public log(): void {
        console.log(this)
    }
    private privateF(): void {
        console.log('private')
    }
}
class Admin extends User {
    private key: string = 'fk33kioin4'
    constructor(public flag: boolean){
        super('Admin', 20);
    }
}
const user = new User('Slon', 120);
user.log();

const admin = new Admin(false);
admin.log();
//

//Enum
enum EnumTest {
    first,
    second = 50,
    third
}
const enumTest: EnumTest = EnumTest.second;
console.log('enum', enumTest);
//

//Object
type Obj = {name: string, age: number, f: () => void, job?: string};

let obj: Obj = {
    name: 'Slon',
    age: 250,
    f: () => {},
}
let obj1: Obj = {
    name: 'Slon1',
    age: 20,
    f: () => {}
}
//

//Array
let arr1: number[] = [1,3,4];
let arr2: Array<number> = [1,3,4];
let arr3: [number, string, boolean] = [1, '1', true]
// 

//Function
function f (param: number = 0):void{
    console.log(param)
}
f();

let f1: (arg: number) => void;
f1 = f;
f1(321); 
//

//namespace
namespace Until {
    export let PI = Math.PI;
    export let E = Math.E;
    let a = 100
}
console.log(Until.PI);
console.log(Until.E);
//namespace end